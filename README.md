# A rapidly deployable, open-source, platform agnostic, collaborative Data Science environment

Built using the best of the open-source world and designed to be highly modular.  This is designed to be used by Data Scientists who wish to hack around on a laptop or deploy to cloud or on-premise infrastructure.

This is not Production ready and lacks the security and access-control you'd expect to see for Production systems.  In addition, it is not yet designed to scale.

## Getting Started

You can follow the next set of instructions to select those containers you require and then start in swarm with Docker-compose.

Alternatively, control individual containers using their Dockerfiles and Docker.

### Start Containers in a Swarm

* Install Docker and Docker-Compose
* Edit docker-compose.yml to remove those containers you don't want
* Ensure the mounted volumes align with your host system (if not using Linux)
* Create some of the mounted folders on your host system (some auto-create when you start the containers)
* Navigate to the folder containing docker-compose.yml in your terminal
* Run ```docker-compose up```
* Feel like a hero!
___

## Useful Docker Commands

```docker ps``` ```docker-compose ps``` list running containers

```docker images``` ```docker-compose images``` list images

```docker logs -f``` ```docker-compose logs -f``` follow logs

```docker-compose up / down``` to start / stop swarm

```docker stop CONTAINER_ID```

```docker exec -it CONTAINER_ID /bin/bash``` to 'SSH' in (assuming bash exists which isn't the case with alpine base images)

```docker save ...``` and ```docker load ...``` to archive and restore images (great for offline systems)

___
### JupyterLab (single user environment)

**Use** by navigating to localhost:8888 and password is **dave1**

___
### Postgres

**Use** by connecting psql or PGAdmin to localhost:5432 

username: **postgres**, password: **dave1**

___
### PG Admin 4

**Use** by connecting to localhost:8080 

default email address: **d@d.s**, default password: **dave1**

___
### Neo4J

**Use** by browsing to ```localhost:7474``` and login 

username: **neo4j**, password: **neo4j** 

(updated admin password and any new users will be retained in mounted volume)

